class fhem {
    
    $fhem_dir = '/opt/fhem'
    
    define install {
        fhem::install_baselibs{ "none": }
        fhem::install_fhem_user{"none": }
        fhem::install_fhem_apt{"none":}
        fhem::install_bluetooth{"none":}
        fhem::config_lograte{'none':}
        fhem::config_auth_proxy{'none':}
        fhem::apache_setup_secure_ssl{'none':}
        fhem::set_permissions{'none':}
        fhem::run{"none":}
    }

    define fhem::enable_apache_module () {
        exec { "/usr/sbin/a2enmod $name" :
            unless => "/bin/readlink -e /etc/apache2/mods-enabled/${name}.load",
            notify => Service[apache2]
        }
    }

    define fhem::enable_apache_config () {
        exec { "/usr/sbin/a2enconf $name" :
            unless => "/bin/readlink -e /etc/apache2/conf-enabled/${name}.conf",
            notify => Service[apache2]
        }
    }

    define fhem::enable_apache_site () {
        exec { "/usr/sbin/a2ensite $name" :
            unless => "/bin/readlink -e /etc/apache2/sites-enabled/${name}.conf",
            notify => Service[apache2]
        }
    }

    define fhem::restart () {
        exec { "/etc/init.d/fhem stop && /etc/init.d/fhem start" : }
    }

    define install_bluetooth {
        package { 'bluetooth':
            ensure => present,
        }
        package { 'bluez':
            ensure => present,
        }
        package { 'blueman':
            ensure => present,
        }
    }

    define apache_setup_secure_ssl {
        file_line { 'Set SSL Protocls':
            path    => '/etc/apache2/mods-enabled/ssl.conf',
            line    => 'SSLProtocol All -SSLv2 -SSLv3',
            match   => '^\s*SSLProtocol\s+.*$',
        }
        file_line { 'Set SSL Ciphers':
            path    => '/etc/apache2/mods-enabled/ssl.conf',
            line    => 'SSLCipherSuite ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4',
            match   => '^\s*SSLCipherSuite\s+.*$',
        }
        file_line { 'Let SSL honor my very special secure ciphers':
            path    => '/etc/apache2/mods-enabled/ssl.conf',
            line    => 'SSLHonorCipherOrder On',
            match   => '^\s*#?\s*SSLHonorCipherOrder\s+.*$',
        }
    }

    define install_baselibs {
        package { 'apt-transport-https':
            ensure => present,
        }
        package { 'libsoap-lite-perl':
            ensure => present,
        }
        package { 'perl':
            ensure => present,
        }
        package { 'libdevice-serialport-perl':
            ensure => present,
        }
        package { 'libio-socket-ssl-perl':
            ensure => present,
        }
        package { 'libjson-perl':
            ensure => present,
        }
        package { 'dfu-programmer':
            ensure => present,
        }
        package { 'dfu-util':
            ensure => present,
        }
        package { 'samba':
            ensure => present,
        }
        package { 'cifs-utils':
            ensure => present,
        }
        package { 'sendEmail':
            ensure => present,
        }
        package { 'etherwake':
            ensure => present,
        }
        package { 'libnet-telnet-perl':
            ensure => present,
        }
        package { 'socat':
            ensure => present,
        }
        package { 'libcrypt-rijndael-perl':
            ensure => present,
        }
        package { 'libdatetime-format-strptime-perl':
            ensure => present,
        }
        package {'usbutils':
            ensure => present,
        }

    }

    define install_fhem_user {
        group {'fhem':
            ensure => present,
        }

        user {'fhem':
            ensure => present,
            home => "$fhem_dir",
            shell => '/bin/false',
            groups => 'fhem',
        }
    }

    define install_fhem_deb {
        file { "$fhem_dir":
            ensure => directory
        }

        exec {'download_fhem':
            command => "/usr/bin/wget -q http://fhem.de/fhem-5.7.deb -O $fhem_dir/fhem.deb",
            creates => "$fhem_dir/fhem.deb"
        }

        file { "$fhem_dir/fhem.deb":
            owner   => root,
            group   => root,
            mode    => 644,
            ensure  => present,
            require => Exec["download_fhem"],
        }

        package {"fhem":
            provider => dpkg,
            ensure => latest,
            source => "$fhem_dir/fhem.deb",
        }
    }

    define install_fhem_apt {
        include apt

        apt::key {"614AC3D65EDE5B423F192B754CC9A0ACBF0E6825":
            ensure => present,
            source => '/root/fhem/fhem/archive.key',
        }

        file {'/etc/apt/sources.list.d/fhem.list':
            path => '/etc/apt/sources.list.d/fhem.list',
            ensure => file,
            source => '/root/fhem/fhem/fhem.list',
        }

        exec { "apt-get update":
            command => "/usr/bin/apt-get update",
        }

        package { 'fhem':
            ensure => present,
        }
    }

    define config_lograte {
        package {'logrotate':
            ensure => present,
        }

        file {'/etc/logrotate.d/fhem':
            path => '/etc/logrotate.d/fhem',
            ensure => file,
            source => '/root/fhem/fhem/logrotate/fhem',
        }
    }

    define config_auth_proxy {
        package {'apache2':
            ensure => present,
        }

        package {'libapache2-mod-proxy-html':
            ensure => present,
        }

        package {'libapache2-mod-encoding':
            ensure => present,
        }

        fhem::fhem::enable_apache_module{'ssl':}
        fhem::fhem::enable_apache_module{'headers':}

        file {"$fhem_dir/certs":
            source => '/root/fhem/fhem/apache2/certs',
            ensure => directory,
            recurse => true,
        }

        file {'/etc/apache2/sites-available/fhem.conf':
            path => '/etc/apache2/sites-available/fhem.conf',
            ensure => file,
            source => '/root/fhem/fhem/apache2/fhem.conf',
        }

        fhem::enable_apache_site{"fhem":}

        file {"$fhem_dir/htuser":
            path => "$fhem_dir/htuser",
            ensure => file,
            source => '/root/fhem/fhem/apache2/htuser',
        }

    /*
        note:
                this block should not be here, but in a seperate fhem.cfg repository which actually exists and I should
                have used instead ...

        file { "$fhem_dir/fhem.cfg":
            ensure => present,
        }->
        file_line {'Add desktop FHEMWEB':
            path => "$fhem_dir/fhem.cfg",
            line => 'define g.web.local.desktop FHEMWEB 40443'
        }->
        file_line {'Add tablet FHEMWEB':
            path => "$fhem_dir/fhem.cfg",
            line => 'define g.web.local.tablet FHEMWEB 40444'
        }->
        file_line {'Add mobile FHEMWEB':
            path => "$fhem_dir/fhem.cfg",
            line => 'define g.web.local.mobile FHEMWEB 40445'
        }
    */

        fhem::enable_apache_module{"proxy": }
        fhem::enable_apache_module{"proxy_http": }
        fhem::enable_apache_module{"proxy_html": }
        fhem::enable_apache_module{"encoding": }
        fhem::enable_apache_module{"xml2enc": }
        fhem::restart{'none':}
    }

    define set_permissions {
        file {"/opt/fhem":
            owner => "fhem",
            group => "fhem",
            mode => "0755",
            recurse => true
        }
    }

    define run {
        service {'apache2':
            ensure => running,
            enable => true,
        }

        service{"fhem":
            ensure => running,
            enable => true,
        }
    }
}

