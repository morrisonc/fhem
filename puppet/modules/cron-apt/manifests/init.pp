class cron-apt {

    define install {

        $base_dir = $assets_dir

        package { 'cron-apt':
            ensure => present,
        }

        file {"/etc/cron-apt/action.d":
            source => "$base_dir/cron-apt/action.d",
            ensure => directory,
            recurse => true,
        }

        file {"/etc/cron-apt/config":
            source => "$base_dir/cron-apt/config",
            ensure => file,
        }
    }
}