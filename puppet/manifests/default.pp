node "hab" {

    $assets_dir = "/opt/puppet/nodes/tulip.local/hab"

    package { 'ntpdate':
        ensure => present,
    }

    include fhem
    fhem::install{"none":}

  /*
    package { 'openssh-server':
        ensure => present,
    }

    package { 'openssh-blacklist':
        ensure => present,
    }
    # fhem

  */

    # system
    package { 'htop':
        ensure => present,
    }

    package { 'tree':
        ensure => present,
    }

    package { 'vim':
        ensure => present,
    }

    package { 'cron-apt':
        ensure => present,
    }
    include cron-apt
    cron-apt::install{"none":}
}


#
