#! /bin/bash

sudo su
aptitude update

# install puppet related
aptitude install -y puppet-common puppet puppet-module-puppetlabs-apache puppet-module-puppetlabs-concat puppet-module-puppetlabs-apt puppet-module-puppetlabs-mysql puppet-module-puppetlabs-ntp puppet-module-puppetlabs-stdlib puppet-module-puppetlabs-inifile puppet-module-puppetlabs-xinetd
aptitude safe-upgrade -y
# install system tools
# nun in puppet
# aptitude install -y htop tree

